$(document).ready(function(){
    $('.modal').modal();
    $('ul.tabs').tabs({

    });
    localStorage.removeItem('delOrderId');
    localStorage.removeItem('selectedDeliveryMedium');
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['userType'] = sessionInfo.userType;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['viewType'] = "NEW"
    var jsonObj = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/orderlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        var jsonData = JSON.stringify(response);
        var dataUse = JSON.parse(jsonData);
        var data = dataUse.data;
        var print = ""
        var totalCount = dataUse.totalCount;
        if(totalCount>0){
          $.each(data, function(i,v){
            print = print+"<tr><td><a href="+feUrl()+"/orderdetails.html?orderId="+v.orderStoreId+">"+v.custName+"</td><td>"+v.phoneNumber+"</td><td>NEW</td><td><a href=javascript:pickupOrder("+v.orderStoreId+")>PICKUP</a></td></tr>";
            var itemList = JSON.stringify(v.itemList);
            var storeName = 'new'+v.orderStoreId;
            localStorage.setItem(storeName,itemList)
          })
        }
        else{
          print = "<div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>Card Title</span><p>Sorry currenty no orders in this section.</p></div></div>";
        }
        $('#newOrders').append(print);
      }
    });
    myObj['viewType'] = "INPROGRESS"
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/orderlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        var jsonData = JSON.stringify(response);
        var dataUse = JSON.parse(jsonData);
        var data = dataUse.data;
        var print = ""
        var totalCount = dataUse.totalCount;
        if(totalCount>0){
          $.each(data, function(i,v){
            print = print+"<tr><td><a href="+feUrl()+"/orderdetails.html?orderId="+v.orderStoreId+">"+v.custName+"</td><td>"+v.phoneNumber+"</td><td>IN-BAG</td><td><a href=javascript:deliverOrder("+v.orderStoreId+")>DELIVER</a></td></tr>";
            var itemList = (JSON.stringify(v.itemList));
            var storeName = 'proc'+v.orderStoreId;
            localStorage.setItem(storeName,itemList)
          })
        }
        else{
          print = "<div class=row><div class=col s12 m6><div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>Card Title</span><p>Sorry currenty no orders in this section.</p></div></div></div></div>";
        }
        $('#inProgOrders').append(print);
      }
    });
    myObj['viewType'] = "COMPLETED";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url : beUrl()+'/orderlist',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        var jsonData = JSON.stringify(response);
        var dataUse = JSON.parse(jsonData);
        var data = dataUse.data;
        var print = ""
        var totalCount = dataUse.totalCount;
        if(totalCount>0){
          $.each(data, function(i,v){
            print = print+"<tr><td><a href="+feUrl()+"/orderdetails.html?orderId="+v.orderStoreId+">"+v.custName+"</td><td>"+v.phoneNumber+"</td><td>DONE</td></tr>";
          })
        }
        else{
          print = "<div class=row><div class=col s12 m6><div class=card blue-grey darken-1><div class=card-content white-text><span class=card-title>Card Title</span><p>Sorry currenty no orders in this section.</p></div></div></div></div>";
        }
        $('#compOrders').append(print);
      }
    });
  });
function pickupOrder(orderId) {
  if(localStorage.getItem('sessionInfo')){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userType'] = sessionInfo.userType;
    myObj['orderStoreId'] = orderId;
    myObj['orderStatus'] = "PICKED";
    var storeName = 'new'+orderId;
    myObj['itemBeingMarked'] =[]
    myObj['itemBeingMarked'] = JSON.parse(localStorage.getItem(storeName));

    var jsonObj = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/updateorderstatus',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        Materialize.toast('Order PICKED Please wait', 4000);
        localStorage.removeItem(storeName);
        setTimeout(function(){
           location.reload();
        }, 500);
      }
    });
  }
  console.log(orderId);
}

function deliverOrder(orderId) {
  if(localStorage.getItem('sessionInfo')){
    var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
    var myObj = {};
    myObj['userId'] = sessionInfo.userId;
    myObj['typeId'] = sessionInfo.typeId;
    myObj['userType'] = sessionInfo.userType;
    myObj['orderStoreId'] = orderId;
    myObj['orderStatus'] = "DELIVER";
    var storeName = 'proc'+orderId;
    var itemsMark = JSON.parse(localStorage.getItem(storeName));
    myObj['itemBeingMarked'] =[]
    myObj['itemBeingMarked'] = itemsMark;
    var jsonObj = JSON.stringify(myObj);
    var head = sessionInfo.basicAuthenticate;
    $.ajax({
      url : beUrl()+'/updateorderstatus',
      type: 'POST',
      dataType:'json',
      processData:false,
      contentType: 'application/json',
      headers : {"basicauthenticate":""+head},
      data:jsonObj,
      success: function(response) {
        Materialize.toast('Order DELIVERED Please wait', 4000);
        localStorage.removeItem(storeName);
        setTimeout(function(){
           location.reload();
        }, 500);
      }
    });
  }
  console.log(orderId);
}
