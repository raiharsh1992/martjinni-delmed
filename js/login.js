$(document).ready(function(){
  $('.modal').modal({
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5,
  })
  var token = getUrlVars()["token"];
  if(token){
    if(token!=""){
      console.log(token);
      sessionStorage.setItem("newToken", token);
      console.log(sessionStorage.getItem("newToken"));
    }
  }
  if(localStorage.getItem('sessionInfo')){
    window.location.replace(feUrl()+"/orders.html");
  }
  else{
    $('#loginOnly').modal('open');
  }
$("#loginbutton").submit(function(event){
    event.preventDefault();
    $("#loginButton").click();
})
$("#loginButton").click(function(){
  event.preventDefault();
  var userName = document.getElementById('username').value;
  var password = document.getElementById('password').value;
  var loginObj = {};
  loginObj['userName']=userName;
  loginObj['password']=password;
  loginObj['mode']="delmed";
  var loginData = JSON.stringify(loginObj);
  $.ajax({
    url : beUrl()+'/login',
    type: 'POST',
    dataType:'json',
    processData:false,
    contentType: 'application/json',
    data:loginData,
    success: function(response) {
      var sessionInfo1 = JSON.stringify(response);
      var sessionInfo = JSON.parse(sessionInfo1);
      if((sessionInfo.userType)=="DM"){
        localStorage.setItem('sessionInfo',sessionInfo1);
        Materialize.toast('Welcome '+sessionInfo.userName+'', 4000);
        setTimeout(function(){
           window.location.replace(feUrl()+"/orders.html");
        }, 500);
      }
      else{
        Materialize.toast('Not authorized', 4000);
      }
    },
    error: function (response) {
      Materialize.toast('Incorrect Username/Password', 4000);
    }
  });
});
})
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}
