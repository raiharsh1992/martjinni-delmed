$('.modal').modal();
$(document).ready(function(){
    if(localStorage.getItem('sessionInfo')){
      var sessionInfo = JSON.parse(localStorage.getItem('sessionInfo'));
      var orderId = getUrlVars()["orderId"];
      var myObj = {};
      myObj['orderId']=orderId;
      myObj['typeId']=sessionInfo.typeId;
      myObj['userId']=sessionInfo.userId;
      myObj['userType']=sessionInfo.userType;
      var jsonObj = JSON.stringify(myObj);
      var head = sessionInfo.basicAuthenticate;
      $.ajax({
        url : beUrl()+'/orderdetails',
        type: 'POST',
        dataType:'json',
        processData:false,
        contentType: 'application/json',
        headers : {"basicauthenticate":""+head},
        data:jsonObj,
        success: function(response) {
          var jsonData = JSON.stringify(response);
          var dataUse = JSON.parse(jsonData);
          $('#orderId').append(dataUse.orderId);
          $('#custName').append(dataUse.custName);
          $('#custNumber').append(dataUse.custNumber);
          $('#addressName').append(dataUse.addName);
          var address = dataUse.addLine1+" "+dataUse.addLine2+" "+dataUse.city+" "+dataUse.state+" "+dataUse.country+" "+dataUse.pincode;
          $('#deliveryAddress').append(address);
          var items = dataUse.itemList;
          $.each(items, function(i,v){
            var print = "<li><div class=collapsible-header><i class=material-icons>local_dining</i>"+v.itemName+"</div><div class=collapsible-body style='padding:0px'><span><table><thead><tr><th>Day</th><th>ETA</th><th>Status</th></tr></thead><tbody id="+v.itemId+"> </tbody></table></span></div></li>";
            $('#itemList').append(print);
            var itemHist = v.itemTranHistory;
            $.each(itemHist, function(k, l){
              var xPrint = "<tr><td>"+l.dayCount+"</td><td>"+l.orderEta+"</td><td>"+l.status+"</td></tr>";
              $('#'+v.itemId).append(xPrint);
            })
          })
        },
        error: function(response){
          Materialize.toast('Not authorized to view the page', 4000)
          setTimeout(function(){
             window.location.replace(feUrl()+"/index.html");
          }, 500);
        }
      });
    }
    else{
      Materialize.toast('Not authorized to view the page', 4000)
      setTimeout(function(){
         window.location.replace(feUrl()+"/index.html");
      }, 500);
    }
  });

  function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
    });
    return vars;
  }
